Dorsett Shepherds Bush is the perfect hotel choice for both business and leisure travellers, offering stylish accommodation for the most discerning guests to relax. Boasting modern architecture and design and overlooking Shepherds Bush Green, the hotel offers 2 restaurants, Jin (?) bar and a spa.

Address: 58 Shepherd's Bush Green, White City, London W12 8QE, UK
Phone: +44 203 262 1026
